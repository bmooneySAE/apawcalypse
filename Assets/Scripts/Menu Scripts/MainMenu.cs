﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public GameObject MainMenuGO;
    public GameObject NewGameGO;
    public GameObject OptionsGO;
    public GameObject CreditsGO;
    public GameObject EscMenuGO;

    public void MainMenuFromNew()
    {
        NewGameGO.SetActive(false);
        MainMenuGO.SetActive(true);
    }

    public void MainMenuFromOptions()
    {
        OptionsGO.SetActive(false);
        MainMenuGO.SetActive(true);
    }

    public void MainMenuFromCredits()
    {
        CreditsGO.SetActive(false);
        MainMenuGO.SetActive(true);
    }

    public void NewGame()
    {
        MainMenuGO.SetActive(false);
        NewGameGO.SetActive(true);
    }

    public void Options()
    {
        MainMenuGO.SetActive(false);
        OptionsGO.SetActive(true);
    }

    public void Credits()
    {
        MainMenuGO.SetActive(false);
        CreditsGO.SetActive(true);
    }

    public void EscScreen()
    {
        EscMenuGO.SetActive(true);
        Time.timeScale = 0;
    }

    public void YesOnEsc()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("GameMenu");
    }

    public void NoOnEsc()
    {
        Time.timeScale = 1;
        EscMenuGO.SetActive(false);
    }

    public void ExitButton()
    {
        EscMenuGO.SetActive(true);
        MainMenuGO.SetActive(false);
        Time.timeScale = 0;
    }

    public void NoMenuEsc()
    {
        MainMenuGO.SetActive(true);
        EscMenuGO.SetActive(false);
        Time.timeScale = 1;
    }

    public void YesMenuEsc()
    {
        Application.Quit();
    }

    public void LevelOne()
    {
        SceneManager.LoadScene("Level01");
    }

    public void LevelTwo()
    {
        SceneManager.LoadScene("Level02");
    }

    public void LevelThree()
    {
        SceneManager.LoadScene("Level03");
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("GameMenu");
    }

    public void update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}

    
