﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// Handles the dragging, and placement of turrets
/// Handles turret upgrading and cost.
/// </summary>

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [Header("GameObjects")]
    public GameObject prefab;
    public Button UpgradeButton;
    public GameObject turretUIUpgradeBox;
    public GameObject UpgradedTurret;
    public GameObject[] placementSlots;
    GameObject activeSlot;
    GameObject hoverPrefab;

    [Header("Variables")]
    public int cost;
    public bool isUpgraded = false;

    GameManager gm;
    // Use this for initialization
    void Start()
    {
        hoverPrefab = Instantiate(prefab);
        RemoveScriptsFromPrefab();
        AdjustPrefabAlpha();
        hoverPrefab.SetActive(false);
        // turretUIUpgradeBox.SetActive(false);

        gm = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    // removes the TurretTargeting script from the turrets while being dragged.
    // this prevents the turrets from firing before being placed.
    void RemoveScriptsFromPrefab()
    {
        Component[] shootingScripts = hoverPrefab.GetComponentsInChildren<TurretTargeting>();
        foreach (Component component in shootingScripts)
        {
            Destroy(component);
        }
    }

    void AdjustPrefabAlpha()
    {
        // collects all meshrenders in the instance of the prefab
        MeshRenderer[] meshRenders = hoverPrefab.GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < meshRenders.Length; ++i)
        {
            // change the colour of the mesh by .5
            Material mat = meshRenders[i].material;
            meshRenders[i].material.color = new Color(mat.color.r, mat.color.g, mat.color.b, 0.5f);
        }
    }

    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
    {
        // Debug.Log(eventData)
    }

    public void OnDrag(PointerEventData eventData)
    {
        // Debug.Log(eventData);
        // create a new raycast
        if (gm.scrap < cost)
        {
            return;
        }
        RaycastHit[] hits;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        hits = Physics.RaycastAll(ray, 300f);
        if (hits != null && hits.Length > 0)
        {
            ShowHoverPrefab(hits);
            var slotIndex = GetSlotIndex(hits);
            if (slotIndex != -1)
            {
                GameObject slotQuad = hits[slotIndex].collider.gameObject;
                activeSlot = slotQuad;
                EnableSlot(slotQuad);
            }
            else
            {
                activeSlot = null;
                DisableAllSlots();
            }
        }
    }

    int GetTerrianColliderIndex(RaycastHit[] hits)
    {
        for (int i = 0; i < hits.Length; ++i)
        {
            // the mouse collides with an object named Plane
            // return that collision
            if (hits[i].collider.gameObject.name.Equals("Plane"))
            {
                return i;
            }
        }
        return -1;
    }

    int GetSlotIndex(RaycastHit[] hits)
    {
        for (int i = 0; i < hits.Length; ++i)
        {
            if (hits[i].collider.gameObject.name.StartsWith("Slot"))
            {
                return i;
            }
        }
        return -1;
    }
    // shows the dragged turret
    void ShowHoverPrefab(RaycastHit[] hits)
    {
        var terrianColliderIndex = GetTerrianColliderIndex(hits);
        if (terrianColliderIndex != -1)
        {
            hoverPrefab.transform.position = hits[terrianColliderIndex].point;
            hoverPrefab.SetActive(true);
        }
        else
        {
            hoverPrefab.SetActive(false);
        }
    }

    void DisableAllSlots()
    {
        foreach (GameObject placementSlots in placementSlots)
        {
            if (placementSlots != null)
            placementSlots.GetComponent<MeshRenderer>().enabled = false;
        }
    }
    // Enables a turret to be placed inside a placement area
    void EnableSlot(GameObject slot)
    {
        foreach (GameObject placementSlots in placementSlots)
        {
            if (placementSlots != null)
                if (slot.name.Equals(placementSlots.name))
            {
                placementSlots.GetComponent<MeshRenderer>().enabled = true;
            }
            else
            {
                placementSlots.GetComponent<MeshRenderer>().enabled = false;
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        /*if (cost > gm.scrap)
        {
            hoverPrefab.SetActive(false);
            return;
        }*/

        // If there is an active slot.. 
        if (activeSlot != null)
            {
                // MeshFilter mf = activeSlot.GetComponent<MeshFilter>();
                Vector3 quadCentre = GetQuadCentre(activeSlot);
                // snaps the prefab to the middle of the quad
                Instantiate(prefab, quadCentre, Quaternion.identity);
                activeSlot.SetActive(false);
                gm.SubtractScrap(cost);
                gm.UpdateScrapText();
        }
        hoverPrefab.SetActive(false);
    }
    // this function snaps the turret to the middle of the placement area
    Vector3 GetQuadCentre(GameObject quad)
    {
        Vector3[] meshVerts = quad.GetComponent<MeshFilter>().mesh.vertices;
        Vector3[] vertRealWordPos = new Vector3[meshVerts.Length];

        for (int i = 0; i < meshVerts.Length; ++i)
        {
            vertRealWordPos[i] = quad.transform.TransformPoint(meshVerts[i]);
        }
        // calculate halfway between the midpoints
        // in otherwords, this finds the middle of the quad
        Vector3 midPoint = Vector3.Slerp(vertRealWordPos[0], vertRealWordPos[1], 0.5f);
        return midPoint;
    }
}
