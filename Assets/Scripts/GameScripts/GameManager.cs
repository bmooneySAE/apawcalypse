﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("Variables")]
    public float timer;  //Is used to time the levels and the spawning
    public Transform spawnLocation;  //Used to spawn enemies
    public int levelNumber; // Used to track the level
    public int enemyWave; //Used to track the spawn wave
    public int currentEnemies;
    public float spawnDelay;  //Holds the time between spawns and allows access in the inspector
    public float startingScrap = 300;

    [Header("Enemy GameObjects")]
    //These are instances of the enemy prefabs
    public GameObject enemyOne;
    public GameObject enemyTwo;
    public GameObject enemyThree;
    public GameObject enemyFour;
    public GameObject enemyFive;

    [Header("TextUI")]
    public Text numWavesText;
    public Text enemiesRemainingText;
    public Text scrapText;
    public Text tenSecTime; //Passes the 10 sec time remaining to the TenSecPanel
    public GameObject tenSecPanel;  //Is the panel that displays when the ten sec feature is enabled.
    public GameObject loosePanelGO;  //Holds the loose panel game object
    public GameObject winPanelGO; //Holds the win panel game object 
    public Text winTime;
    public Text winScrap;
    public Text winTenSecTime;
    public Text totalScore;

    [Header("Game Objects")]
    public GameObject cargoShip;
    public GameObject bigExplosion;
    public GameObject cargoShipContainer;



    [Header("Test Values")]
    //private EnemyWaves enemyWaves;  //This is a list of int arrays holding the spawns
    //make private
    //**UNUSED** public bool waitTen;   //This bool indicates if we are in the 10 second time feature
    //**UNUSED** public bool spawning; //Tells unity if the waves are currently spawning
    //Make Private
    public EnemyWaves enemyWaves;
    private Transform cargoPosition;
    private float waitTime = 10;
    //Make private
    public float waitedTime = 0;
    public bool enemiesDestroyed;
    private int spawns; //Holds the number of enemies that has spawned
    private float lastSpawn; //Holds the time since last spawn
    private int spawnsRemaining;
    //make private
    public float timeBetweenSpawns;
    public float scrap;
    public float totalLevelTime = 0; //Is used to calculate the total time taken to complete the level. Used for calculating score
    public float tenSecSkipTime = 0; //Holds the total time the player has skipped the ten second function
    private AudioManager audioManager;
    enum systemState { Pause, TenSec, Spawning, Combat, Win, Loose };
    systemState currentState;
    private systemState lastState;
    private Scoring scoring = new Scoring();





    // Use this for initialization
    void Start()
    {
        audioManager = GameObject.Find("GameManager").GetComponent<AudioManager>();

        currentState = systemState.Pause;

        UpdateEnemiesRemaining();
        UpdateCurrentRound();

        enemyWaves = GetComponent<EnemyWaves>();
        enemyWave = 0;

        scrap = startingScrap;
        UpdateScrapText();

        cargoPosition = cargoShip.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Time.timeScale = 10;
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            Time.timeScale = 1;
        }

        /*if (Input.GetKeyDown(KeyCode.Escape))
        {
            currentState = systemState.Pause;
        }*/

        switch (currentState)
        {
            case systemState.Pause:
                //print("Aloha");
                break;

            case systemState.Spawning:
                //print("Spawning");

                totalLevelTime += Time.deltaTime;

                spawnsRemaining = enemyWaves.levelWaves[enemyWave - 1].Count;

                if (spawnsRemaining <= 0)
                {
                    currentState = systemState.Combat;
                }

                if (timeBetweenSpawns > spawnDelay)
                {
                    timeBetweenSpawns -= spawnDelay + Time.deltaTime;
                    SpawnWave();
                }
                else
                    timeBetweenSpawns += Time.deltaTime;

                break;


            case systemState.TenSec:
                //print("tenSec");  //Test

                tenSecTime.text = (System.Convert.ToInt32(waitTime - waitedTime + 0.49f)).ToString();

               /* if (Input.GetKeyDown(KeyCode.Space))
                {
                    CompleteTenSec();
                }*/

                if (waitedTime < waitTime)
                {
                    waitedTime += Time.deltaTime;
                }
                else
                {
                    CompleteTenSec();
                }
                break;

            case systemState.Combat:
                //print("Combat");
                totalLevelTime += Time.deltaTime;

                if (currentEnemies <= 0)
                {
                    enemiesDestroyed = true;
                }

                if (enemiesDestroyed)
                {
                    if (enemyWave < 10)
                    {
                        StartTenSec();
                    }
                    else
                        Win();
                }

                break;

            case systemState.Win:
                //print("WINNING!");
                break;

            case systemState.Loose:
                //print("DOH!");
                break;
        }


        



        //If in wait 10 period, add time.deltaTime to the waitTime.
        /*
         * //OLD Code replaced in StateManager
         * if (waitTen)
        {
            if (waitedTime < waitTime)
            {
                waitedTime += Time.deltaTime;
            }
            else
            {
                enemyWave++;
                UpdateCurrentRound();
                spawnsRemaining = enemyWaves.levelWaves[enemyWave - 1].Count;
                SpawnWave();
                //spawnsRemaining = enemyWaves.levelOne[enemyWave].Count;
                
                //print(spawnsRemaining);
                waitTen = false;
                spawning = true;
                
            }
        }*/


        //OLD Code replaced in state manager
        /*if (!waitTen && enemiesDestroyed)
        {
            waitTen = true;
            waitedTime = 0;
            
        }*/


        //OLD Code replaced in state manager
        /*if (spawning)
        {
            
            if (spawnsRemaining <= 0)
            {
                spawning = false;
            }

            if (timeBetweenSpawns > spawnDelay)
            {
                timeBetweenSpawns -= spawnDelay + Time.deltaTime;
                SpawnWave();
            }
            else
                timeBetweenSpawns += Time.deltaTime;

        }*/
    }

    void SpawnWave()
    {

        int randomSpawn = Random.Range(0, spawnsRemaining - 1);
        GameObject enemyToSpawn = getNextEnemy(enemyWaves.levelWaves[enemyWave - 1][randomSpawn]);
        enemyWaves.levelWaves[enemyWave - 1].RemoveAt(randomSpawn);
        Instantiate(enemyToSpawn, spawnLocation.position, Quaternion.identity);
        currentEnemies++;
        spawnsRemaining--;
        UpdateEnemiesRemaining();


        //print(spawnsRemaining);


        /*TEST CODE
         * if (spawns < 5)
        {
            if (lastSpawn > spawnDelay)
            {
                Instantiate(enemyOne, spawnLocation.position, Quaternion.identity);
                //print("Instantiated");
                lastSpawn = spawnDelay - lastSpawn + Time.deltaTime;
                //spawns++;
                currentEnemies.Add(spawns);
                UpdateEnemiesRemaining();
            }
            else
            {
                lastSpawn += Time.deltaTime;
                //print("Increase spawn time " + lastSpawn);
            }
        }
        */
    }

    public void BuyTurret(string turretName)
    {

        // float _scrap = GetComponent<Scrap>().scrap;
        //GameObject[] turrets = GameObject.FindGameObjectsWithTag("Turret"); 
        {
            // foreach (GameObject turret in turrets)
            {
                //float mgCost = GetComponent<MachineTurret>().turretCost;
                //float gtCost = GetComponent<GatlingTurret>().turretCost;
            }
        }


    }

    public void UpgradeTurret(string turretName)
    {

    }

    public int LevelComplete(int levelNumber)
    {
        return 0;
    }

    public void LevelFailed()
    {
        //Vector3 explosion = new Vector3(81.05f, -1.66f, -15.5f);
        currentState = systemState.Loose;
        Instantiate(bigExplosion, cargoShipContainer.transform.position, Quaternion.identity);
        print(cargoShipContainer.transform.position);
        Destroy(cargoShip.gameObject);

        loosePanelGO.SetActive(true);
    }

    void SetScores(int score)
    {

    }

    void Win()
    {
        currentState = systemState.Win;
        winPanelGO.SetActive(true);
        winScrap.text = scrap.ToString();
        winTenSecTime.text = System.Convert.ToInt32(tenSecSkipTime).ToString();
        int tempTime = System.Convert.ToInt32(totalLevelTime);
        int seconds = tempTime % 60;
        int minutes = (tempTime - seconds) / 60;
        winTime.text = minutes + ":" + seconds;

        totalScore.text = scoring.CalculateScores(tempTime, tenSecSkipTime, scrap).ToString();
    }

    void UI()
    {

    }

    void WaitTen()
    {

    }

    public void EnemyDestroyed()
    {
        currentEnemies--;
        UpdateEnemiesRemaining();
    }

    public void UpdateEnemiesRemaining()
    {

        enemiesRemainingText.text = "Enemies Remaining: " + currentEnemies.ToString();
    }

    /* NOW UNUSED
    GameObject getSpawn(int waveNumber)
    {
        int enemyWaveNumber = enemyWaves.levelOne[waveNumber - 1][Random.Range(0, enemyWaves.levelOne[0].Count)];
        GameObject enemyToReturn = getNextEnemy(enemyWaves.levelOne[waveNumber - 1][enemyWaveNumber]);
        enemyWaves.levelOne[waveNumber - 1].Remove(enemyWaveNumber);
        return enemyToReturn;      
    } */

    void UpdateCurrentRound()
    {
        numWavesText.text = "Current Wave: " + enemyWave.ToString();
    }

    GameObject getNextEnemy(int enemyNumber)
    {
        GameObject enemyToReturn = null;

        switch (enemyNumber)
        {
            case 1:
                enemyToReturn = enemyOne;
                break;
            case 2:
                enemyToReturn = enemyTwo;
                break;
            case 3:
                enemyToReturn = enemyThree;
                break;
            case 4:
                enemyToReturn = enemyFour;
                break;
            case 5:
                enemyToReturn = enemyFive;
                break;
        }

        return enemyToReturn;
    }

    public void AddScrap(float scrapToAdd)
    {
        scrap += scrapToAdd;
        UpdateScrapText();
    }

    public void SubtractScrap(float scrapToSubtract)
    {
        scrap -= scrapToSubtract;

        if (scrap <= 0)
        {
            scrap = 0;
        }
        UpdateScrapText();
    }

    public void UpdateScrapText()
    {
        // update scrap text 
        scrapText.text = "Scrap: " + scrap.ToString();
    }


    public void StartTenSec()
    {
        currentState = systemState.TenSec;
        waitedTime = 0;
        tenSecPanel.SetActive(true);
    }

    public void CompleteTenSec()
    {
        print("CLICKED");
        tenSecPanel.SetActive(false);
        tenSecSkipTime += waitTime - waitedTime;
        enemyWave++;
        UpdateCurrentRound();
        spawnsRemaining = enemyWaves.levelWaves[enemyWave - 1].Count;
        if (enemyWaves == null)
        {
            return;
        }
        enemiesDestroyed = false;
        currentState = systemState.Spawning;
    }

    public void StartGame()
    {
        currentState = systemState.TenSec;
    }
}
