﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Scoring
{ 

    public float timeModifier;
    public float skipModifier;
    public float scrapModifier;


    public int CalculateScores(float time, float skipTime, float scrapRemaining)
    {
        float timeBonus = 1000 - time;
        float skipBonus;

        if (skipTime > 90)
            skipBonus = 100;
        else
            skipBonus = skipTime + 10;

        return System.Convert.ToInt32((timeBonus * timeModifier) + (skipBonus * skipModifier) + (scrapRemaining * scrapModifier));
    }

}
