﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This class handles all the audio for the game. Any SFX needing running should be stored in the <AudioFiles> list and called by name
//The plan is to change this to an event system next week

public class AudioManager : MonoBehaviour
{
    [System.Serializable]
    public class AudioFiles
    {
        public string Name;
        public AudioClip clip;
        [Range(0, 1)]
        public float volume;
    }

    public AudioSource source;

    public List<AudioFiles> soundFX;

	// Use this for initialization
	void Start ()
    {
    

    }
	
	// Update is called once per frame
	void Update () {
        
        //This was testing code
        /*
        if (Input.GetKeyDown(KeyCode.Space))
        {
            source.clip = soundFX.Find(x => x.Name=="turretOneFire").clip;
            source.Play();
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            source.clip = soundFX.Find(x => x.Name == "turretTwoFire").clip;
            source.Play();
        }
        */
	}

    //This function will play the sound effect if it finds one with name that is passed to it.
    public void PlaySFX(string soundClip)
    {
        var sound = soundFX.Find(x => x.Name == soundClip);
        source.clip = sound.clip;
        source.volume = sound.volume; //PersistentData.instance.SFXVolume * sound.volume;
        source.Play();
    }
}
