﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetReadyScript : MonoBehaviour
{
    public GameManager gameManagerScript;
    public Text waitSeconds;

    private float timer = 5;
        

	// Use this for initialization
	void Start ()
    {
        gameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (timer > 0)
        {
            timer -= Time.deltaTime;
            waitSeconds.text = (System.Convert.ToInt32(timer + 0.49f)).ToString();
        }
        
        else
        {
            timer = 0;
            gameObject.SetActive(false);
            gameManagerScript.StartGame();
        }
	}
}
