﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWaves : MonoBehaviour {

    public List<int> wave1_01;
    public List<int> wave1_02;
    public List<int> wave1_03;
    public List<int> wave1_04;
    public List<int> wave1_05;
    public List<int> wave1_06;
    public List<int> wave1_07;
    public List<int> wave1_08;
    public List<int> wave1_09;
    public List<int> wave1_10;

    public List<int>[] levelWaves;



    // Use this for initialization
    void Start ()
    {
        levelWaves = new List<int>[] { wave1_01, wave1_02, wave1_03, wave1_04, wave1_05, wave1_06, wave1_07, wave1_08, wave1_09, wave1_10 };
    }
	
}
