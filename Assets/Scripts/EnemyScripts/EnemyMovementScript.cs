﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyMovementScript : MonoBehaviour {

    public float speed;
    public float maxMs;
    public GameObject gameManager; //Holds a reference to the game manager

    GameManager gameManagerScript;
    Transform target;

    // Use this for initialization
    void Start ()
    {
        target = GameObject.Find("Waypoint01").transform;
        gameManager = GameObject.Find("GameManager");
        gameManagerScript = gameManager.GetComponent<GameManager>();
        speed = maxMs;
    }
	
	// Update is called once per frame
	void Update ()
    {
        transform.LookAt(target);
        Vector3 move = Vector3.MoveTowards(this.transform.position, target.position, speed * Time.deltaTime);
        transform.position = move;

        if (speed < maxMs)
        {
            Invoke("UnStunMe", 4f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Waypoint")
        {
            target = GameObject.Find("Waypoint" + getNumberFromString(other.name)).transform;
            print(getNumberFromString(other.name));
        }

        if (other.tag=="Finish")
        {
            gameManagerScript.LevelFailed();
        }
        
        
    }

    string getNumberFromString(string name)
    {
        int waypointNumber = System.Convert.ToInt32(new string(name.Where(c => char.IsDigit(c)).ToArray()));
        waypointNumber++;
        //print(waypointNumber);

        if (waypointNumber < 10)
            return ("0" + waypointNumber).ToString();
        else
            return waypointNumber.ToString();
    }

    public void Stunned(float stunTime)
    {
        speed = 0;
    }

    void UnStunMe()
    {   
        speed = maxMs;
    }
}
