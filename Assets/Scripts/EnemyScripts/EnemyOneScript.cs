﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyOneScript : MonoBehaviour
{
    public GameObject gameManager;//Holds the gameManager game object  
    public float enemyHP = 100; //Holds the float for the HP. This is assigned through the inspector
    public GameObject explosion; //Holds the explosion game object to call when destroyed

    AudioManager audioManager;
    GameManager gm;

	// Use this for initialization
	void Start ()
    {
        gameManager = GameObject.Find("GameManager");
        audioManager = gameManager.GetComponent<AudioManager>();
        gm = FindObjectOfType<GameManager>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            TakeDamage(10);
            print("Damage");
        }
    }

    public void TakeDamage(float damage)
    {
        if (gameObject.active)
        {
            enemyHP -= damage;
            if (enemyHP > 0)
                audioManager.PlaySFX("enemyHurt");
            if (enemyHP <= 0)
            {
                //enemyHP = 0;

                audioManager.PlaySFX("catExplode");
                Instantiate(explosion, transform.position, Quaternion.identity);
                gm.AddScrap(5);
                gm.EnemyDestroyed();
                gameObject.SetActive(false);
                Destroy(gameObject);
            }
        }
    }
}
