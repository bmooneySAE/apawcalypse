﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Transform target;
    public GameObject impactEffect;

    public float _damage;
    public float explosionRadius = 0f;

    TurretTargeting tt;
    public float speed = 70f;
	// Use this for initialization
	void Start ()
    {
        tt = GetComponent<TurretTargeting>();
        if (GetComponent<TurretTargeting>() == null)
        {
            return;
        }
        _damage = tt.damage;
	}
	
	// Update is called once per frame
	void Update ()
    {
        // checks if the bullets target has been destroyed, if it has destroy the bullet
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        var dir = target.position - transform.position;
        var distanceThisFrame = speed * Time.deltaTime;
        if (dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }
        // effectvely tells the bullet where the enemy is in the world space
        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        transform.LookAt(target);
	}

    public void Seek(Transform _target)
    {
        // passes the target from the turret to the bullet
        target = _target;
    }

    void HitTarget()
    {
        // creates on hit effects
        GameObject effectIns = (GameObject)Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(effectIns, 1f);
        Destroy(gameObject);
        if (explosionRadius > 0f)
        {
            Explode();
        }
        else // if the explosionRadius is less 0 then just do normal damage, no need to run explode function
        {
            target.SendMessage("TakeDamage", _damage);
        }
    }
    void Explode()
    {
        // creates a sphere where ever the bullet hits
        // sphere size is dictated by explosionRadius - so anything the sphere colliders with takes damage 
        Collider[] hitObjects = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach (Collider collider in hitObjects)
        {
            if (collider.tag == "enemy")
            {
                collider.SendMessage("TakeDamage", _damage);
            }
        }
    }
}
