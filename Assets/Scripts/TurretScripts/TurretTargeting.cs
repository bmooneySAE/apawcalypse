﻿using UnityEngine;
using System.Collections;

public class TurretTargeting : MonoBehaviour
{
    [Header("Variables")]
    public float range = 50f;
    public float turnSpeed = 10f;
    public float fireRate = 1f;
    private float fireCountdown = 0f;
    public string enemyTag = "enemy";
    public float damage;

    [Header("Unity Setup Fields")]
    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    public Transform partOfTurretToRotate;
    private Transform target;

    AudioManager am;

    // Use this for initalization
    void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
    }

    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        var shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemy in enemies) // searches for the positions of each enemy
        {
            var distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }
        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
        }
        else
        {
            target = null;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            return;
        }

        LockOnTarget();

        if (fireCountdown <= 0)
        {
            Fire();
            fireCountdown = 1f / fireRate;
        }
        fireCountdown -= Time.deltaTime;
    }

    void LockOnTarget()
    {
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partOfTurretToRotate.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        partOfTurretToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    void Fire()
    {
        // Create the Bullet from the Bullet Prefab
        var bulletGo = (GameObject)Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
        Bullet bullet = bulletGo.GetComponent<Bullet>();
        bullet._damage = damage;

        // am.PlaySFX("turretShooting1");
        // if there is a bullet, make it seek an enemy.
        if (bullet != null)
        {
            bullet.Seek(target);
        }
    }
}