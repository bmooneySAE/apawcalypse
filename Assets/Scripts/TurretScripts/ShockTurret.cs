﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShockTurret : MonoBehaviour
{
    [Header("Variables")]
    public float damage;
    private bool isStunning;

    [Header("GameObjects")]
    public GameObject shockEffect;

    [HideInInspector]
    EnemyMovementScript ems;
    SphereCollider sc;

    // Use this for initialization
    void Start ()
    {
        ems = GetComponent<EnemyMovementScript>();
        InvokeRepeating("SettingDamageArea", 1f, 1f);
        InvokeRepeating("ResetDamageArea", 1f, 1f);
        // StartCoroutine("SettingDamageArea", 5f,);
        // StartCoroutine("ResetDamageArea", 5f);
        sc = FindObjectOfType<SphereCollider>();
    }
	
	// Update is called once per frame
	void Update ()
    {
   
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "enemy")
        {
            other.SendMessage("TakeDamage", damage);
            other.SendMessage("Stunned", 3f);
        }
        else
        {
            return;
        }

        if (other == null)
        {
            return;
        }
        GameObject effectIns = (GameObject)Instantiate(shockEffect, transform.position, transform.rotation);
        Destroy(effectIns, 3f);
    }
    // enables the sphere collider damaging everything inside it and stunning it
    // then every 5 seconds Invokes ResetDamageArea and disables it.
    void SettingDamageArea()
    {
        sc.enabled = true;
        isStunning = true;
    }

    void ResetDamageArea()
    {
        sc.enabled = false;
        isStunning = false;
    }
}
