﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLine : MonoBehaviour {

    private LineRenderer lineRenderer;
    private float counter;
    private float dist;

    [Header("Origin and Destination")]
    public Transform origin;
    public Transform destination;

    public float lineDrawSpeed = 1f;
    /*Speed = Distance/Time
     - Need to input the time taken between origin and destination
     - 
    
    */
	// Use this for initialization
	void Start ()
    {
        lineRenderer = GetComponent<LineRenderer>();
        if (lineRenderer != null)
        {
            lineRenderer.SetPosition(0, origin.position);
            lineRenderer.SetWidth(.65f, .65f);
        }

        dist = Vector3.Distance(origin.position, destination.position);

	}
	
	// Update is called once per frame
	void Update ()
    {
        if (counter < dist)
        {
            counter += .1f / lineDrawSpeed;

            float x = Mathf.Lerp(0, dist, counter);

            Vector3 pointA = origin.position;
            Vector3 pointB = destination.position;

            Vector3 pointAlongLine = x * Vector3.Normalize(pointB - pointA) + pointA;
            if (lineRenderer != null)
            lineRenderer.SetPosition(1, pointAlongLine);
        }
	}
}
